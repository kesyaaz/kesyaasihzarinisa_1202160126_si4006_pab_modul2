package com.example.kezz.kesyaasihzarinisa_1202160126_si4006_pab_modul2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    Button btnBeli, beliTiket;
    TextView hasil, topUp, plhTanggal1, plhWaktu1, plhTanggal2, plhWaktu2;
    EditText editBeliTiket;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    DatePickerDialog.OnDateSetListener mDateSetListener2;
    TimePickerDialog.OnTimeSetListener mTimeSetListener;
    TimePickerDialog.OnTimeSetListener mTimeSetListener2;
    private int mYear, mMonth, mDay, mHour, mMinute, hargaTiket;
    private String[] travel;
    private String selesaiBooking, jakarta, cirebon, bekasi;
    private Switch switchPulangPergi;
    private Spinner spinnerKota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
//        travel[] = res.getStringArray(R.string.travel);
        hasil = (TextView) findViewById(R.id.Saldo);
        final Intent in = getIntent();
        btnBeli = (Button) findViewById(R.id.beliTiket);
        beliTiket = (Button) findViewById(R.id.beliTiket);
        topUp = (TextView) findViewById(R.id.topUp);
        plhTanggal1 = (TextView) findViewById(R.id.plhTanggal1);
        plhTanggal2 = (TextView) findViewById(R.id.plhTanggal2);
        plhWaktu1 = (TextView) findViewById(R.id.plhWaktu1);
        plhWaktu2 = (TextView) findViewById(R.id.plhWaktu2);
        switchPulangPergi = (Switch)findViewById(R.id.switchPulangPergi);
        spinnerKota = (Spinner)findViewById(R.id.spinnerKota);
        editBeliTiket = (EditText) findViewById(R.id.editBeliTiket);

        plhTanggal2.setVisibility(View.GONE);
        plhWaktu2.setVisibility(View.GONE);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.travel, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKota.setAdapter(adapter);

        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Maaf, saldo kamu kurang. Top up saldo dulu, gih.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        topUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Masukkan Jumlah Saldo");

                final EditText input = new EditText(MainActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                builder.setView(input);
                builder.setPositiveButton("TAMBAH SALDO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hasil.setText("Rp " + input.getText().toString());
                        Toast.makeText(getApplicationContext(), "Top up berhasil!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                android.R.string.cancel, Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

        plhTanggal1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mYear = cal.get(Calendar.YEAR);
                mMonth = cal.get(Calendar.MONTH);
                mDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                        mDateSetListener,
                        mYear,mMonth,mDay);
                dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dateDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = mMonth + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                plhTanggal1.setText(date);
            }
        };

        plhWaktu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mHour = cal.get(Calendar.HOUR_OF_DAY);
                mMinute = cal.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                        mTimeSetListener,
                        mHour, mMinute, false);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay + " : " + minute;
                plhWaktu1.setText(time);
            }
        };

        switchPulangPergi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchPulangPergi.isChecked() == true){

                    plhTanggal2.setVisibility(View.VISIBLE);
                    plhWaktu2.setVisibility(View.VISIBLE);

                    plhTanggal2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                                    mDateSetListener2,
                                    mYear,mMonth,mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "/" + month + "/" + year;
                            plhTanggal2.setText(date);
                        }
                    };

                    plhWaktu2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                                    mTimeSetListener2,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });

                    mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            plhWaktu2.setText(time);
                        }
                    };

                }
                else if(!switchPulangPergi.isChecked()){
                    plhTanggal2.setVisibility(View.GONE);
                    plhWaktu2.setVisibility(View.GONE);

                    plhTanggal2.setText("Pilih Tanggal");
                    plhWaktu2.setText("Pilih Waktu");

                    plhTanggal1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                                    mDateSetListener,
                                    mYear,mMonth,mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "/" + month + "/" + year;
                            plhTanggal1.setText(date);
                        }
                    };

                    plhWaktu1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                                    mTimeSetListener,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });

                    mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            plhWaktu1.setText(time);
                        }
                    };
                }
            }
        });

        beliTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer a = 0;
                Integer hargaTotal = 0;
                selesaiBooking = getString(R.string.selesaiBooking);

                if(switchPulangPergi.isChecked()){
                    a = Integer.parseInt(editBeliTiket.getText().toString()) * 2;
                }
                else if(!switchPulangPergi.isChecked()){
                    a = Integer.parseInt(editBeliTiket.getText().toString());
                }

                try{
                    Integer jumlahSaldo = Integer.parseInt(hasil.getText().toString());
                    Integer jumlahTiket = a;

                    Intent intent = new Intent(MainActivity.this, CheckoutAcivity.class);

                    if(spinnerKota.getSelectedItem().equals("Jakarta (Rp. 85.000)")){
                        hargaTiket = 85000;
                        hargaTotal = a * hargaTiket * jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Saldo Anda tidak mencukupi!").setNegativeButton(
                                    "Retry", null).create().show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), selesaiBooking, Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerKota.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", plhTanggal1.getText().toString());
                            intent.putExtra("waktuBerangkat", plhWaktu1.getText().toString());
                            intent.putExtra("tanggalPulang", plhTanggal2.getText().toString());
                            intent.putExtra("waktuPulang", plhWaktu2.getText().toString());
                            intent.putExtra("jumlahTiket", editBeliTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", hasil.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }

                    else if(spinnerKota.getSelectedItem().equals("Cirebon (Rp. 150.000)")){
                        hargaTiket = 150000;
                        hargaTotal = a * hargaTiket * jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Saldo Anda Tidak Mencukupi!").setNegativeButton(
                                    "Retry", null).create().show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), selesaiBooking, Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerKota.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", plhTanggal1.getText().toString());
                            intent.putExtra("waktuBerangkat", plhWaktu1.getText().toString());
                            intent.putExtra("tanggalPulang", plhTanggal2.getText().toString());
                            intent.putExtra("waktuPulang", plhTanggal2.getText().toString());
                            intent.putExtra("jumlahTiket", editBeliTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", hasil.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }

                    else if(spinnerKota.getSelectedItem().equals("Bekasi (Rp. 70.000)")){
                        hargaTiket = 70000;
                        hargaTotal = a * hargaTiket * jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Saldo Anda Tidak Mencukupi!").setNegativeButton(
                                    "Retry", null).create().show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), selesaiBooking, Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerKota.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", plhTanggal1.getText().toString());
                            intent.putExtra("waktuBerangkat", plhWaktu1.getText().toString());
                            intent.putExtra("tanggalPulang", plhTanggal2.getText().toString());
                            intent.putExtra("waktuPulang", plhWaktu2.getText().toString());
                            intent.putExtra("jumlahTiket", editBeliTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", hasil.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }
                }
                catch (Exception e){
                    AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                    alertBuild.setMessage("Mohon lengkapi data di atas!").setNegativeButton(
                            "Retry", null).create().show();
                }
            }
        });
    }
}
