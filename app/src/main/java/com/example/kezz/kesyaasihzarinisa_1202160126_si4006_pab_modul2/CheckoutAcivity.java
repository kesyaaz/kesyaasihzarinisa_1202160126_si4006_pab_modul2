package com.example.kezz.kesyaasihzarinisa_1202160126_si4006_pab_modul2;


import android.content.Intent;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CheckoutAcivity extends AppCompatActivity {

    TextView viewTujuan, viewTanggalBerangkat, viewWaktuBerangkat,
            viewTanggalPulang, viewWaktuPulang, viewJumlahTiket, viewHargaTotal,
            viewSaldo, viewSisaSaldo;

    Button konfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_acivity);

        viewTujuan = (TextView)findViewById(R.id.viewTujuan);
        viewTanggalBerangkat = (TextView)findViewById(R.id.viewTanggalBerangkat);
        viewWaktuBerangkat = (TextView)findViewById(R.id.viewWaktuBerangkat);
        viewTanggalPulang = (TextView)findViewById(R.id.viewTanggalPulang);
        viewWaktuPulang= (TextView)findViewById(R.id.viewWaktuPulang);
        viewJumlahTiket = (TextView)findViewById(R.id.viewJumlahTiket);
        viewHargaTotal = (TextView)findViewById(R.id.viewHargaTotal);
        konfirmasi = (Button)findViewById(R.id.btnKonfirmasi);

        Intent in = getIntent();

        String tujuan = in.getStringExtra("daerahTujuan");
        String tanggalBerangkat = in.getStringExtra("tanggalBerangkat");
        String waktuBerangkat = in.getStringExtra("waktuBerangkat");
        String tanggalPulang = in.getStringExtra("tanggalPulang");
        String waktuPulang = in.getStringExtra("waktuPulang");
        String jumlahTiket = in.getStringExtra("jumlahTiket");
        String hargaTotal = in.getStringExtra("hargaTotal");
        String saldo = in.getStringExtra("saldo");
        String sisaSaldo = in.getStringExtra("sisaSaldo");

        viewTujuan.setText(tujuan);
        viewTanggalBerangkat.setText(tanggalBerangkat);
        viewWaktuBerangkat.setText(waktuBerangkat);
        viewTanggalPulang.setText(tanggalPulang);
        viewWaktuPulang.setText(waktuPulang);
        viewJumlahTiket.setText(jumlahTiket);
        viewHargaTotal.setText("Rp " + hargaTotal);
        viewSaldo.setText(saldo);
        viewSisaSaldo.setText(sisaSaldo);

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Terima kasih. Semoga selamat sampai tujuan!", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }
}
